#ifndef SCROLLEDIMAGECOMPONENT_H
#define SCROLLEDIMAGECOMPONENT_H

#include "ScanProject.h"
#include "wx/wx.h"
#include "doscan.h"

class ScrolledImageComponent : public wxScrolledWindow {
  wxBitmap* bitmap;
  ScanProject *project;
  int w,h;
  public:

  ScrolledImageComponent(wxWindow* parent, wxWindowID id, wxString image_path) : wxScrolledWindow(parent, id) {
    wxImage image(image_path);
    if(!image.IsOk()) {
      wxMessageBox(wxT("there was an error loading the image"));
      return;
    }

    w = image.GetWidth();
    h = image.GetHeight();

    /* init scrolled area size, scrolling speed, etc. */
    SetScrollbars(1,1, w, h, 0, 0);

    bitmap = new wxBitmap( image );
    parent->SetSize(w,h);
    project = new ScanProject("/tmp/testdir");
  }

  ~ScrolledImageComponent() {
    delete bitmap;
  }

  void OnDraw(wxDC& dc) {
    dc.DrawBitmap(*bitmap, 0, 0, false);
  }

  void rightClick(wxMouseEvent &event) {
    wxClientDC dc(this);
    PrepareDC(dc);
    wxPoint here = event.GetLogicalPosition(dc);
    char buf[128];
    //sprintf(buf, "./scan.pl %i %i", here.x, this->h-here.y);
    list<scandata> results;
    wxBusyCursor wait;
    time_t now = time(nullptr);

    results = doscan();
    project->addResults(results, here.x, this->h-here.y, now);
  }

  DECLARE_EVENT_TABLE()
};


#endif
