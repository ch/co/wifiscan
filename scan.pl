#!/usr/bin/perl

use warnings;
use strict;
use Data::Dumper;

my $threshold=-70;


open(LIST, "/sbin/iwlist scan 2>&1 |") or die "could not run iwlist";

my %report;
my $essid;
my $mac;

my $wanted_ssid="ChemNet";

while(<LIST>) {
	if(/Address\:(.*)/) { $mac=$1; } 
	elsif(/ESSID\:\"(.*)\"/) { $report{$mac}->{"essid"} = $1; }
#elsif(/Signal level=(.*) dBm/) { $report{$mac}->{"quality"} = $1; }
elsif(/Quality=(.*)\/70 /) { $report{$mac}->{"quality"} = $1; }
}

my $best_signal=-1000;
my $total=0;

foreach $mac (keys %report) {
	if(lc $report{$mac}->{"essid"} eq lc $wanted_ssid) {
		$total += $report{$mac}->{"quality"};
		if($report{$mac}->{"quality"} > $best_signal) {
			$best_signal = $report{$mac}->{"quality"};
		}
	}
}

print $ARGV[0].", ".$ARGV[1].", ".$best_signal.", ".$total."\n";
