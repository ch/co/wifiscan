#ifndef SCANPROJECT_H
#define SCANPROJECT_H

#include <string>
#include <sqlite3.h>
#include <list>
#include "scandata.h"

using namespace std;

class ScanProject {
  public:
    string projectDir;

    ScanProject(string projectDir);
    ~ScanProject();

    void addResults(list<scandata> results, int xpos, int ypos, int timestamp);

  private:
    sqlite3 *currentDb;

    void mkProjectDir();
    void addDb();

};

#endif
