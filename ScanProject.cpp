#include "ScanProject.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <iostream>
#include <errno.h>
#include <unistd.h>
#include <sqlite3.h>
#include <ctime>
#include <list>
#include "scandata.h"
#include <string>
#include <cstring>

using namespace std;

ScanProject::ScanProject(string projectDir) {
  this->projectDir = projectDir;
  this->mkProjectDir();
  this->addDb();
}

ScanProject::~ScanProject() {
  sqlite3_close(this->currentDb);
}

void ScanProject::mkProjectDir() {
  int status;
  if(0 != access(this->projectDir.c_str(), F_OK)) {
    if(ENOTDIR == errno) {
      std::cout << this->projectDir << " exists but is not a directory." << std::endl;
      return;
    }

    if(ENOENT == errno) {
      cout << "dir not found, making it." << endl;
      status = mkdir(this->projectDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }
  }
}

void ScanProject::addDb() {
  int status;
  time_t now = time(nullptr);
  string filename = this->projectDir;
  filename.append("/");
  filename.append(to_string(now));
  filename.append(".db");
  char *errMsg;

  status = sqlite3_open(filename.c_str(), &(this->currentDb));

  string sql = "create table scandata ( \
                timestamp int, \
                xpos int, \
                ypos int, \
                ssid char(32), \
                macaddr char(17), \
                quality int)";

  status = sqlite3_exec(this->currentDb, sql.c_str(), nullptr, nullptr, &errMsg);
}

void ScanProject::addResults(list<scandata> results, int xpos, int ypos, int timestamp) {
  char *sql;
  char *errorMsg;
  int status;
  sqlite3_stmt *stmt;
  const char *ptest;

  sql = "insert into scandata (timestamp, xpos, ypos, ssid, macaddr, quality) \
         values (?, ?, ?, ?, ?, ?)";

  status = sqlite3_prepare(this->currentDb, sql, strlen(sql), &stmt, &ptest);

  list<scandata>::iterator it;
  for(it=results.begin(); it != results.end(); it++) {
    printf("%s %s %i %i\n", it->ssid.c_str(), it->macaddr.c_str(), it->quality, it->level);
    sqlite3_bind_int(stmt, 1, timestamp);
    sqlite3_bind_int(stmt, 2, xpos);
    sqlite3_bind_int(stmt, 3, ypos);
    sqlite3_bind_text(stmt, 4, it->ssid.c_str(), strlen(it->ssid.c_str()), nullptr);
    sqlite3_bind_text(stmt, 5, it->macaddr.c_str(), strlen(it->macaddr.c_str()), nullptr);
    sqlite3_bind_int(stmt, 6, it->quality);
    sqlite3_step(stmt);
    sqlite3_reset(stmt);
  }

  sqlite3_finalize(stmt);

}
