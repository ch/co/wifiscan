set dgrid3d
set pm3d
unset key
unset xlabel
unset ylabel
unset xtics
unset ytics
unset ztics
set xrange [0:800]
set yrange [0:463]
set view 0,0
set multiplot
set lmargin at screen 0.05
set rmargin at screen 0.85
set tmargin at screen 0.9
set bmargin at screen 0.1
unset colorbox
set style fill transparent solid 0.9
set colorbox vertical user origin 0.9, 0.1 size 0.05, 0.8
#set cblabel offset -1,0
splot '180-before' u 1:2:3 w l nosurf 
unset colorbox
plot "image.png" binary filetype=png w rgbalpha
unset multiplot
pause -1
