#ifndef SCANDATA_H
#define SCANDATA_H

#include <string>

using namespace std;

struct scandata {
  string ssid;
  string macaddr;
  int quality;
  int level;
};

#endif
