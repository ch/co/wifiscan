CXX = $(shell wx-config --cxx) -g -pg

PROGRAM = scan

LIBS=-liw -lsqlite3

OBJECTS = $(PROGRAM).o ScanProject.o WifiScanFrame.o ScrolledImageComponent.o

.SUFFIXES: .o .cpp

.cpp.o :
	$(CXX) -c `wx-config --cxxflags` -o $@ $< -std=c++11

all: $(PROGRAM)

$(PROGRAM): $(OBJECTS) doscan.o
	$(CXX) -o $(PROGRAM) $(OBJECTS) doscan.o `wx-config --libs` $(LIBS)

doscan: 
	$(CXX) -c doscan.cpp 

clean:
	rm -f *.o $(PROGRAM)
