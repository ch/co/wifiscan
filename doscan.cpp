#include <iwlib.h>
#include "scandata.h"
#include <list>

using namespace std;

list<scandata> doscan() {
  wireless_scan_head head;
  wireless_scan *result;
  iwrange range;
  list<scandata> data;
  char buffer[128];

  int sock;
  /* Open socket to kernel */
  sock = iw_sockets_open();

  /* Get some metadata to use for scanning */
  if (iw_get_range_info(sock, "wlan0", &range) < 0) {
    printf("Error during iw_get_range_info. Aborting.\n");
    exit(2);
  }

  /* Perform the scan */
  char if_name[] = "wlan0";
  if (iw_scan(sock, if_name, range.we_version_compiled, &head) < 0) {
    printf("Error during iw_scan. Aborting.\n");
    exit(2);
  }

  /* Traverse the results */
  result = head.result;
  while (NULL != result) {
    scandata tmp;
    tmp.ssid = result->b.essid;
    tmp.quality = result->stats.qual.qual;
    tmp.macaddr = iw_sawap_ntop(&(result->ap_addr), buffer);
    tmp.level = result->stats.qual.level;
    data.push_back(tmp);
    result = result->next;
  }
  return data;
}

