#include "WifiScanFrame.h"
#include "wx/wx.h"
#include "ScrolledImageComponent.h"
#include "ScanProject.h"

enum {
  MENU_FILE_IMAGE_OPEN,
};

WifiScanFrame::WifiScanFrame(const wxString& title, const wxPoint& pos, const wxSize& size) : wxFrame(NULL, wxID_ANY, title, pos, size) {
  menuBar = new wxMenuBar();
  fileMenu = new wxMenu();
  fileMenu->Append(MENU_FILE_IMAGE_OPEN, wxT("open"));
  fileMenu->Append(wxID_EXIT, wxT("Quit"));
  menuBar->Append(fileMenu, wxT("File"));
  SetMenuBar(menuBar);

}

void WifiScanFrame::OnExit(wxCommandEvent& event) {
  if(imageIsLoaded == true) {
    delete my_image;
  }
  this->Close();
}

void WifiScanFrame::fileOpenImage(wxCommandEvent& event) {
  wxFileDialog* dlg = new wxFileDialog(this, wxT("Pick file"), wxEmptyString, wxEmptyString, wxT("Images|*.jpg;*.png"),wxFD_OPEN, wxDefaultPosition);

  if(dlg->ShowModal() == wxID_OK) {
    imagePath = dlg->GetPath();
    this->loadImage();
  }
  dlg->Destroy();
}

void WifiScanFrame::loadImage() {
  if(imageIsLoaded == true) {
    delete my_image;
  }
  my_image = new ScrolledImageComponent(this, wxID_ANY, this->imagePath );
  imageIsLoaded = true;
}


BEGIN_EVENT_TABLE(WifiScanFrame, wxFrame)
  EVT_MENU(wxID_EXIT, WifiScanFrame::OnExit)
  EVT_MENU(MENU_FILE_IMAGE_OPEN, WifiScanFrame::fileOpenImage)
END_EVENT_TABLE()
