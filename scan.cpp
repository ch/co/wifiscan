#include "wx/wx.h"
#include "doscan.h"
#include <iostream>
#include "scandata.h"
#include <list>
#include "ScanProject.h"
#include "WifiScanFrame.h"

class MyApp: public wxApp {
  WifiScanFrame *frame;
  public:

  bool OnInit() {
    wxInitAllImageHandlers();
    //wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    frame = new WifiScanFrame(wxT("WiFi Scanner"), wxPoint(50,50), wxSize(950,950));

//    sizer->Add(my_image, 1, wxALL | wxEXPAND, 0);
  //  frame->SetSizer(sizer);

    frame->Show();
    return true;
  } 
};

IMPLEMENT_APP(MyApp)
