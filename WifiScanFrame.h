#ifndef WIFISCANFRAME_H
#define WIFISCANFRAME_H

#include "wx/wx.h"
#include "ScrolledImageComponent.h"

class WifiScanFrame : public wxFrame {
  public:
    WifiScanFrame(const wxString& title, const wxPoint& pos, const wxSize& size);

  private:
    void OnExit(wxCommandEvent& event);
    void fileOpenImage(wxCommandEvent& event);

    ScrolledImageComponent* my_image;
    bool imageIsLoaded;
    void loadImage();
    wxMenuBar* menuBar;
    wxMenu* fileMenu;

    wxString imagePath;

  protected:
    DECLARE_EVENT_TABLE();
};

#endif
